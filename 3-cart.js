const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


// Q1. Find all the items with price more than $65.

function productPrice(products){
    const item = Object.entries(products[0]);
    let nestedItems;
    const result = item.filter(([key,each]) => {
        if(Array.isArray(each)){
            const nested = each.filter((nestedEach) => {
                const array = Object.entries(nestedEach);
                //console.log(array)
                return array[0][1].price.replace('$', '') > 65;
            });
            nestedItems = nested;
            
        } else {
            return each.price.replace('$','') > 65
        }
    });

    return nestedItems.concat(result)
}

console.log(productPrice(products));


// function productsPricesGraterThan65(products) {
//     const productsArray = Object.entries(products[0])
//     //console.log(productsArray)

//     return productsArray.filter(([, each]) => {
//         if (Array.isArray(each)) {
//             const nestedObjects = each.filter((e) =>{
//                 const arr = Object.entries(e)
//                 return arr[0][1].price.replace('$','') > 65
                
//             })
//             console.log(nestedObjects)
//             // return [...nestedObjects]
//         } else {
//             return each.price.replace('$', '') > 65
//         }
//     })
// }

// console.log(productsPricesGraterThan65(products))

// Q2. Find all the items where quantity ordered is more than 1.
// Q.3 Get all items which are mentioned as fragile.
// Q.4 Find the least and the most expensive item for a single quantity.
// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)
